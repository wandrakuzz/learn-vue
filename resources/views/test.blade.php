@extends('master')
@section('section')
  <div id="root">
    <input type="text" id="input" v-model="newName">
    <input type="text" id="input" v-model="newAge">
    <button v-on:click="addName">Add Name</button>

    <table >
      <thead>
        <tr>
          <th>Name</th>
          <th>Age</th>
        </tr>
      </thead>
      <tbody v-for="p in people">
        <trv>
          <td>@{{p.name}}</td>
          <td>@{{p.age}}</td>
        </tr>
      </tbody>

    </table>
  </div>


@endsection

@section('scripts')
  <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
  <script>

    new Vue({
      el: '#root',
      data: {
        newName: '',
        newAge:'',
        people: [
          {name:'wan', age:'18'},
          {name:'karim', age:'20'},
        ]
      },
      methods: {
        addName(){
          this.people.push(this.newName);

          this.newName = '';
          this.newAge = '';
        }


      }
    })

    Vue.config.devtools = true
    </script>
@endsection
